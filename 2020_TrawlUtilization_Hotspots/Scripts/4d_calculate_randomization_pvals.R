# carry out randomization test for utilization ratio 

library(ggplot2)
library(sf)
library(tidyr)
library(plyr)
library(dplyr)

options(dplyr.summarise.inform = FALSE)

reLev<-c("-1", "-1 to -0.5", "-0.5 to 0", "0 to 0.5", "0.5 to 1", "1")
spLev<-c("cwcs","coralNoPen", "seapen", "sponge")

# Set Working Directory
setwd("D:/Documents/Projects/SpongeCoralHotspots_GroundfishTrawl/2020_TrawlUtilization_Hotspots/")

source("Scripts/ggplot-custom-theme.R")
source("Scripts/cwcs-plot-functions.R")

# Set output location
shpout<-c("3_Output/EncounterRatios/shp/")
txtout<-c("3_Output/EncounterRatios/txt/")
figout<-c("3_Output/EncounterRatios/fig/")

# load utilization/encounter ratio script
source("Scripts/re_ru_fn.R")

# cutoff
cutoff <- 10

## --- 
# -------------
# ## bring in randomization results
store_bind1<-readRDS(paste0(txtout, "Ru_Randomization_meanAcrossIterations_fn_100_encrate_wt.rdata"))
full_bind<-readRDS(paste0(txtout,"Ru_Randomization_detailOfIterations_fn_100_encrate_wt.rdata"))

# bring in real results
fopps<-readRDS(paste0(txtout, "EncUtilRatio_byFishingOpp_allgroups_cutoff10.rdata"))
foppscwcs<-fopps[fopps$group=="cwcs",]
meta<-readRDS(paste0(txtout,"CWCS_catch_by_FishingOpp_ReBin_expanded_fn_new.rdata"))
metacwcs<-meta[meta$group=="cwcs",]
metacwcs$run<-"real"

# # --- TEST 1 - re by ru
# # get mean re for each random iteration
# store_bind<-store_bind1[,c("RuBin", "Re_mean")]
# store_bind$run<-"random"
# 
# # combine real and random
# compare<-rbind.data.frame(metacwcs[,c("RuBin","Re_mean","run")], store_bind)
# compare$run<-as.factor(compare$run)
# 
# # summarized results from randomization
# res$run<-"random"
# names(res)[names(res)=="mean_Re_mean"]<-"Re_mean"
# 
# #combine metadata
# means<-rbind(metacwcs[,c("RuBin", "Re_mean", "run")],res[,c("RuBin", "Re_mean","run")])
# 
# # #Re by RuBin, comapre randomization to real data
# # png(paste0(figout, "Re_by_Ru_cwcs_cutoff_",cutoff,"_random_vs_real_fn2.png"), width=14,height=8, res=250, units="cm")
# # ggplot(compare, aes(x=RuBin, y=Re_mean, fill=run))+
# #   geom_hline(yintercept = 0, linetype=2)+geom_hline(yintercept = 0.5, linetype=2)+geom_hline(yintercept = -0.5, linetype=2)+
# #   geom_boxplot(outlier.size = 0.3)+theme_kg()+
# #   theme(axis.text.x=element_text(size=8))+
# #   xlab("Utilization Ratio Bin")+ylab("Mean Encounter Ratio")+
# #   geom_point(data=means, aes(x=RuBin, y=Re_mean, fill=run),shape=4,position=position_dodge(width=.75))+
# #   ggtitle(paste0("Mean Re of fishing opps (real), and average of mean Re across iterations (random)\n(nFe Fopp cutoff = ",cutoff,")"))
# # dev.off()
# 
# 
# ###
# # statistical test
# # statistic: for each Re category, proportion of iterations, where the # of fishing opps in that category exceeds the true number
# full_bind<- full_bind %>% mutate(ReBin=case_when(
#   Re==(-1) ~ "-1",
#   Re>(-1) & Re<=(-0.5) ~ "-1 to -0.5",
#   Re>(-0.5) & Re<=0 ~ "-0.5 to 0",
#   Re>0 & Re<=0.5 ~"0 to 0.5",
#   Re>0.5 & Re<1 ~ "0.5 to 1",
#   Re==1 ~"1",
#   TRUE~NA_character_))
# 
# #summarize randomized data - number of opportunities per ReBin
# iter<-ddply(full_bind, c("ReBin", "iteration"),summarize, nOpps=n_distinct(CLUSTER_ID) )
# 
# # prep real data, get same - number of opps per ReBin
# # FLAG _ fill in the dummy values only for the plots - can't have to remember to check for nas all the time
# real<-foppscwcs[!is.na(foppscwcs$ReBin),]
# real1<-ddply(real, c("ReBin"), summarize, nOpp_real=n_distinct(CLUSTER_ID))
# real1$pOpp_real<-round(real1$nOpp_real/sum(real1$nOpp_real),3)
# names(real1)[names(real1)=="ReBin_cwcs"]<-"ReBin"
# 
# #combine real and simulated data
# iter<-merge(iter, real1, by="ReBin")
# 
# # count number of times the iterated (randomized) count per bin exceeds the real count per bin
# iter$exceed<-(iter$nOpps>iter$nOpp_real)
# check<-ddply(iter, c("ReBin", "nOpp_real","pOpp_real"),summarize,nIter_exceed=sum(exceed), pIter_exceed=sum(exceed)/1000)
# 
# check$ReBin<-factor(check$ReBin, levels=reLev)
# check<-check[order(check$ReBin),]
# check # pExceed is the p value. Looks like the bins -1 to -0.5 and 0.5 to 1 
# 
# 
# # plot histograms of distributions
# bins<-unique(iter$ReBin)
# i<-6
# hist(iter$nOpps[iter$ReBin==bins[i]], main=paste0("ReBin = ",bins[i], "\np = ", check$pExceed[check$ReBin==bins[i]]), xlab="Number of Opportunities in bin (n=1000 iterations)")
# abline(v=iter$nOpps_real[iter$ReBin==bins[i]], col="red")
# 
# # try with broader groups: put negative and strong negative together, positive and strong positive\
# real_group<-real %>% mutate(ReBin_group=case_when(
#   Re<=(-0.5) ~ "-1 to -0.5",
#   Re>(-0.5) & Re<=0 ~ "-0.5 to 0",
#   Re>0 & Re<=0.5 ~"0 to 0.5",
#   Re>0.5  ~ "0.5 to 1",
#   TRUE~NA_character_))
# 
# full_group<-full_bind %>% mutate(ReBin_group=case_when(
#   Re<=(-0.5) ~ "-1 to -0.5",
#   Re>(-0.5) & Re<=0 ~ "-0.5 to 0",
#   Re>0 & Re<=0.5 ~"0 to 0.5",
#   Re>0.5  ~ "0.5 to 1",
#   TRUE~NA_character_))
# 
# full_group<-ddply(full_group, c("ReBin_group", "iteration"),summarize, nOpps=n_distinct(CLUSTER_ID) )
# 
# real_group<-ddply(real_group, c("ReBin_group"), summarize, nOpp_real=n_distinct(CLUSTER_ID))
# real_group$pOpp_real<-round(real_group$nOpp_real/sum(real_group$nOpp_real),3)
# 
# #combine real and simulated data
# iter_group<-merge(full_group, real_group, by="ReBin_group")
# 
# # count number of times the iterated (randomized) count per bin exceeds the real count per bin
# iter_group$exceed<-(iter_group$nOpps>iter_group$nOpp_real)
# check_group<-ddply(iter_group, c("ReBin_group", "nOpp_real","pOpp_real"),summarize,nIter_exceed=sum(exceed), pIter_exceed=sum(exceed)/1000)
# 
# check_group$ReBin_group<-factor(check_group$ReBin_group, levels=reLev)
# check_group<-check_group[order(check_group$ReBin_group),]
# check_group # so cannot say Re is not randomly distributed.




# ---- what about encounter rate?
#summarize randomized data - number of opportunities per RuBin
head(store_bind1)

enc<-store_bind1[,c("RuBin", "iteration", "mean_enc_rate_")]
enc$mean_enc_rate_[is.na(enc$mean_enc_rate_)]<-0

head(enc)

head(metacwcs)
names(metacwcs)[!names(metacwcs)=="RuBin"]<-paste0(names(metacwcs)[!names(metacwcs)=="RuBin"], "_real")
names(metacwcs)<-gsub("_real_real", "_real", names(metacwcs))

enc<-merge(enc, metacwcs[,c("RuBin", "mean_enc_rate__real")], by="RuBin")

head(enc)

#need to reformat
enc<-enc[order(enc$RuBin, enc$iteration),]

enc1<-enc %>% pivot_wider(id_cols = c(iteration), names_from=RuBin, values_from=c(mean_enc_rate_, mean_enc_rate__real))
names(enc1)  
names(enc1)<-gsub("\\s","_",names(enc1))
names(enc1)<-gsub("\\-","neg",names(enc1))
names(enc1)<-gsub("\\.","p",names(enc1))
names(enc1)                  

#calc differences between adjacent pairs of RU cats
enc1$abd_strneg<-abs(enc1$mean_enc_rate__neg1-enc1$mean_enc_rate__neg1_to_neg0p5)
enc1$strneg_neg<-abs(enc1$mean_enc_rate__neg1_to_neg0p5-enc1$mean_enc_rate__neg0p5_to_0)
enc1$neg_pos<-abs(enc1$mean_enc_rate__neg0p5_to_0-enc1$mean_enc_rate__0_to_0p5)
enc1$pos_strpos<-abs(enc1$mean_enc_rate__0_to_0p5-enc1$mean_enc_rate__0p5_to_1)
enc1$strpos_new<-abs(enc1$mean_enc_rate__0p5_to_1-enc1$mean_enc_rate__1)

enc1$abd_strneg_real<-abs(enc1$mean_enc_rate__real_neg1-enc1$mean_enc_rate__real_neg1_to_neg0p5)
enc1$strneg_neg_real<-abs(enc1$mean_enc_rate__real_neg1_to_neg0p5-enc1$mean_enc_rate__real_neg0p5_to_0)
enc1$neg_pos_real<-abs(enc1$mean_enc_rate__real_neg0p5_to_0-enc1$mean_enc_rate__real_0_to_0p5)
enc1$pos_strpos_real<-abs(enc1$mean_enc_rate__real_0_to_0p5-enc1$mean_enc_rate__real_0p5_to_1)
enc1$strpos_new_real<-abs(enc1$mean_enc_rate__real_0p5_to_1-enc1$mean_enc_rate__real_1)

names(enc1)

# count number of times the iterated (randomized) change in encounter rate exceeds the real change
enc1$exceed_abd_strneg<-enc1$abd_strneg>enc1$abd_strneg_real
enc1$exceed_strneg_neg<-enc1$strneg_neg>enc1$strneg_neg_real
enc1$exceed_neg_pos<-enc1$neg_pos>enc1$neg_pos_real
enc1$exceed_pos_strpos<-enc1$pos_strpos>enc1$pos_strpos_real
enc1$exceed_strpos_new<-enc1$strpos_new>enc1$strpos_new_real


# # count number of times the iterated (randomized) change in encounter rate exceeds the real change
nIter_exceed_abd_strneg<-sum(enc1$exceed_abd_strneg)
nIter_exceed_strneg_neg<-sum(enc1$exceed_strneg_neg)
nIter_exceed_neg_pos<-sum(enc1$exceed_neg_pos)
nIter_exceed_pos_strpos<-sum(enc1$exceed_strneg_neg)
nIter_exceed_strpos_new<-sum(enc1$exceed_strpos_new)

nIter<-length(unique(enc1$iteration))

p_exceed_abd_strneg<-nIter_exceed_abd_strneg/nIter
p_exceed_strneg_neg <-nIter_exceed_strneg_neg/nIter
p_exceed_neg_pos    <-nIter_exceed_neg_pos/nIter
p_exceed_pos_strpos <-nIter_exceed_pos_strpos/nIter
p_exceed_strpos_new <-nIter_exceed_strpos_new/nIter

check_enc<-cbind.data.frame(comparison= c("abd_strneg", "strneg_neg", "neg_pos", "pos_strpos","strpos_new"),
          nIter_exceed=c(nIter_exceed_abd_strneg, nIter_exceed_strneg_neg, nIter_exceed_neg_pos, nIter_exceed_pos_strpos, nIter_exceed_strpos_new),            p_exceed=c(p_exceed_abd_strneg, p_exceed_strneg_neg, p_exceed_neg_pos, p_exceed_pos_strpos, p_exceed_strpos_new))

check_enc # pExceed is the p value. Looks like the middle bins are significant

check_enc2<-ddply(enc, c("RuBin" ),summarize,mean_enc_rate_real=mean(mean_enc_rate__real))
df2

# ---- what about non-zero encounter wt?
#summarize randomized data - number of opportunities per RuBin
# FLAG - catch should be non-zero catch (encounter size)
nzwt<-store_bind1[,c("RuBin", "iteration", "mean_mean_nonzero_catch_")]
nzwt$mean_mean_nonzero_catch_[is.na(nzwt$mean_mean_nonzero_)]<-0

nzwt<-merge(nzwt, metacwcs[,c("RuBin", "mean_mean_nonzero_catch__real")], by="RuBin")

head(nzwt)

#need to reformat
nzwt<-nzwt[order(nzwt$RuBin, nzwt$iteration),]

nzwt1<-nzwt %>% pivot_wider(id_cols = c(iteration), names_from=RuBin, values_from=c(mean_mean_nonzero_catch_, mean_mean_nonzero_catch__real))
names(nzwt1)  
names(nzwt1)<-gsub("\\s","_",names(nzwt1))
names(nzwt1)<-gsub("\\-","neg",names(nzwt1))
names(nzwt1)<-gsub("\\.","p",names(nzwt1))
names(nzwt1)                  

#calc differnzwtes between adjacent pairs of RU cats
nzwt1$abd_strneg<-abs(nzwt1$mean_mean_nonzero_catch__neg1-nzwt1$mean_mean_nonzero_catch__neg1_to_neg0p5)
nzwt1$strneg_neg<-abs(nzwt1$mean_mean_nonzero_catch__neg1_to_neg0p5-nzwt1$mean_mean_nonzero_catch__neg0p5_to_0)
nzwt1$neg_pos<-abs(nzwt1$mean_mean_nonzero_catch__neg0p5_to_0-nzwt1$mean_mean_nonzero_catch__0_to_0p5)
nzwt1$pos_strpos<-abs(nzwt1$mean_mean_nonzero_catch__0_to_0p5-nzwt1$mean_mean_nonzero_catch__0p5_to_1)
nzwt1$strpos_new<-abs(nzwt1$mean_mean_nonzero_catch__0p5_to_1-nzwt1$mean_mean_nonzero_catch__1)

nzwt1$abd_strneg_real<-abs(nzwt1$mean_mean_nonzero_catch__real_neg1-nzwt1$mean_mean_nonzero_catch__real_neg1_to_neg0p5)
nzwt1$strneg_neg_real<-abs(nzwt1$mean_mean_nonzero_catch__real_neg1_to_neg0p5-nzwt1$mean_mean_nonzero_catch__real_neg0p5_to_0)
nzwt1$neg_pos_real<-abs(nzwt1$mean_mean_nonzero_catch__real_neg0p5_to_0-nzwt1$mean_mean_nonzero_catch__real_0_to_0p5)
nzwt1$pos_strpos_real<-abs(nzwt1$mean_mean_nonzero_catch__real_0_to_0p5-nzwt1$mean_mean_nonzero_catch__real_0p5_to_1)
nzwt1$strpos_new_real<-abs(nzwt1$mean_mean_nonzero_catch__real_0p5_to_1-nzwt1$mean_mean_nonzero_catch__real_1)

names(nzwt1)

# count number of times the iterated (randomized) change in nzwtounter rate exceeds the real change
nzwt1$exceed_abd_strneg<-nzwt1$abd_strneg>nzwt1$abd_strneg_real
nzwt1$exceed_strneg_neg<-nzwt1$strneg_neg>nzwt1$strneg_neg_real
nzwt1$exceed_neg_pos<-nzwt1$neg_pos>nzwt1$neg_pos_real
nzwt1$exceed_pos_strpos<-nzwt1$pos_strpos>nzwt1$pos_strpos_real
nzwt1$exceed_strpos_new<-nzwt1$strpos_new>nzwt1$strpos_new_real


# # count number of times the iterated (randomized) change in nzwtounter rate exceeds the real change
nzwt_nIter_exceed_abd_strneg<-sum(nzwt1$exceed_abd_strneg)
nzwt_nIter_exceed_strneg_neg<-sum(nzwt1$exceed_strneg_neg)
nzwt_nIter_exceed_neg_pos<-sum(nzwt1$exceed_neg_pos)
nzwt_nIter_exceed_pos_strpos<-sum(nzwt1$exceed_strneg_neg)
nzwt_nIter_exceed_strpos_new<-sum(nzwt1$exceed_strpos_new)

nIter<-length(unique(nzwt1$iteration))

nzwt_p_exceed_abd_strneg<- nzwt_nIter_exceed_abd_strneg/nIter
nzwt_p_exceed_strneg_neg <-nzwt_nIter_exceed_strneg_neg/nIter
nzwt_p_exceed_neg_pos    <-nzwt_nIter_exceed_neg_pos/nIter
nzwt_p_exceed_pos_strpos <-nzwt_nIter_exceed_pos_strpos/nIter
nzwt_p_exceed_strpos_new <-nzwt_nIter_exceed_strpos_new/nIter

check_nzwt<-cbind.data.frame(comparison= c("abd_strneg", "strneg_neg", "neg_pos", "pos_strpos","strpos_new"),
                            nIter_exceed=c(nzwt_nIter_exceed_abd_strneg, nzwt_nIter_exceed_strneg_neg, nzwt_nIter_exceed_neg_pos, nzwt_nIter_exceed_pos_strpos, nzwt_nIter_exceed_strpos_new),            p_exceed=c(nzwt_p_exceed_abd_strneg, nzwt_p_exceed_strneg_neg, nzwt_p_exceed_neg_pos, nzwt_p_exceed_pos_strpos, nzwt_p_exceed_strpos_new))

check_nzwt # pExceed is the p value. Looks like the middle bins are significant



# count number of times the iterated (randomized) change in encounter rate exceeds the real change
check_nzwt2<-ddply(nzwt, c("RuBin" ),summarize,mean_mean_nonzero_catch__real=mean(mean_mean_nonzero_catch__real))

check_nzwt2 # pExceed is the p value. Looks like the bins 0.5 to 1 
df3


#write results
write.csv(check_enc,paste0(txtout, "ru_randomization_encRate.csv"),row.names=F)
write.csv(check_nzwt,paste0(txtout, "ru_randomization_encWeightMean.csv"),row.names=F)
write.csv(check_wt,paste0(txtout, "ru_randomization_totalWeight.csv"),row.names=F)


# Look at by-Tow RU
head(metacwcs)
