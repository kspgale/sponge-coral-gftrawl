library(tidyverse)
library(sf)
library(reshape)

dat<-readRDS("D:/Documents/Projects/SpongeCoralHotspots_GroundfishTrawl/2020_TrawlUtilization_Hotspots/1_InputData/data_processed/txt/CWCS_wtByTow_optA_lines_allkm_07-19_wCluster.rdata") # save the cleaned up trip-catch table (long)
names(dat)[1]<-"FISHING_EV"

gear<-read.csv("D:/Documents/Projects/SpongeCoralHotspots_GroundfishTrawl/2020_TrawlUtilization_Hotspots/1_InputData/data_gfdata/GF_FE_TRAWL_SPECS.txt")
head(gear)
names(gear)[1]<-"FISHING_EV"

dat<-st_drop_geometry(st_as_sf(dat))
head(dat)
nrow(dat) #108502

dat2<-merge(dat, gear, by="FISHING_EV", all.x=T) 
nrow(dat2)  #108502

head(dat2)

locs<-dat2 %>% select(FISHING_EV, TRAWL_NET_TYPE_CODE, DOOR_SPREAD, LatStart, LonStart) %>% distinct()
nrow(locs)
plot(locs$LonStart, locs$LatStart )
points(locs$LonStart[locs$TRAWL_NET_TYPE_CODE==3], locs$LatStart[locs$TRAWL_NET_TYPE_CODE==3], col="red" )

hist(dat2$DOOR_SPREAD)
hist(dat2$WING_SPREAD)
unique(dat2$NET_GEOMETRY_UNIT_CODE)
unique(dat2$TRAWL_NET_TYPE_CODE)
nrow(subset(dat2, TRAWL_NET_TYPE_CODE=="3"))

dat2a<-melt(dat2[,c("FISHING_EV", "year", "cwcs","TRAWL_NET_TYPE_CODE","DOOR_SPREAD" ,"length")], id.vars=c("FISHING_EV", "year", "cwcs","TRAWL_NET_TYPE_CODE","length"), measure_vars=c("DOOR_SPREAD" ))

dat2a$kgkm<-dat2a$cwcs/dat2a$length
dat2a$kgkm_bywidt<-dat2a$kgkm/dat2a$value


dat2a %>% nrow() #108502
dat2a %>% filter(value==0| is.na(value)) %>% nrow() #50838
 

dat2a %>% filter(cwcs>0) %>% nrow() #3703
dat2a %>% filter((value==0 | is.na(value)) & cwcs>0) %>% nrow() #1429
dat2a %>% filter(value!=0 & !is.na(value) & cwcs>0) %>% nrow() #2274

#door spread  values not available for 
50838/108502 #47% of tows
1429/3703 #39% of encounters

quantile(((dat2a %>% filter(value!=0 & !is.na(value) & cwcs>0) %>% select(value) )$value/3.281),c(0.1, 0.94))
hist(((dat2a %>% filter(value!=0 & !is.na(value) & cwcs>0) %>% select(value) )$value/3.281), breaks=c(seq(0,300, by=10)))



dat3<-dat2a %>%  mutate(.,timePeriod=case_when(year>2011~ "late", year<2011~"early")) %>% 
  # filter(!is.na(TRAWL_NET_TYPE_CODE), !is.na(timePeriod))%>% 
  group_by(variable) %>% summarize( mean=mean(value, na.rm=T), min=min(value[value>0], na.rm=T), max=max(value, na.rm=T) ) 
dat3
hist(dat2$DOOR_SPREAD/3.281, breaks=c(seq(0,300, by=25)))
dat2$DOOR_SPREAD_M<-dat2$DOOR_SPREAD/3.281
nrow(dat2[dat2$DOOR_SPREAD_M<=75,])/nrow(dat2)  # 94%
nrow(dat2[dat2$DOOR_SPREAD_M<=100,])/nrow(dat2) # 97% 
max(dat2$DOOR_SPREAD_M, na.rm=T) #272 m


wMid<-dat2 %>%  group_by(CLUSTER_ID, RUiBin, timePeriod) %>%   summarize(nTow=length(unique(FISHING_EV)),nMid=length(unique(FISHING_EV[TRAWL_NET_TYPE_CODE==3])), nMidTowOver185=length(unique(FISHING_EV[length>1850&TRAWL_NET_TYPE_CODE==3])), propMid=nMid/nTow) %>% filter(nTow>10, nMid>0) %>% arrange(.,-propMid)

wMid
length(wMid$propMid) #331/373 FOs have some mid in them


