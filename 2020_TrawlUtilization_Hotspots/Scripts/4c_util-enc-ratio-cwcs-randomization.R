# Randomization of cwcs catch to compare encounter and utilization ratios
# assumption is that if encounters are random, then ru and re would be relatively similar
# to test how encounters are distributed, shuffle cwcs catch within fishing events and re-calculate Re and Ru

library(ggplot2)
library(sf)
library(tidyr)
library(plyr)
library(dplyr)

options(dplyr.summarise.inform = FALSE)

reLev<-c("-1", "-1 to -0.5", "-0.5 to 0", "0 to 0.5", "0.5 to 1", "1")
spLev<-c("cwcs","coralNoPen", "seapen", "sponge")

# Set Working Directory
setwd("D:/MyDocuments/Projects/SpongeCoralHotspots_GroundfishTrawl/2020_TrawlUtilization_Hotspots/")

source("Scripts/ggplot-custom-theme.R")
source("Scripts/cwcs-plot-functions.R")

# Set output location
shpout<-c("3_Output/EncounterRatios/shp/")
txtout<-c("3_Output/EncounterRatios/txt/")
figout<-c("3_Output/EncounterRatios/fig/")

# load utilization/encounter ratio script
source("Scripts/re_ru_fn.R")

# this version of the cwcs (attached to clusters) doesn't have 2012 (transition year) 
cwcs0<-readRDS("1_InputData/data_processed/txt/CWCS_wtByTow_optA_lines_07-11_13-19_inClusters.rdata")@data
nrow(cwcs0) #93977
cwcs1<-cwcs0 %>% mutate(timePeriod=as.factor(timePeriod))

cwcs1<-cwcs1 %>% select(CLUSTER_ID, cwcs, timePeriod, FISHING_EV, year)

all_opps<-unique(cwcs1$CLUSTER_ID)
all_opps<-all_opps[order(all_opps)]
years<-unique(cwcs1$year)

# --- Load fishing opportunities and species-line data within the clusters
opp_shp<-st_read("2_FishingOpportunities/1_DataFromRobyn/0711-1319_max18_5km/polygons/ExportPoly_withAttributes.shp")
names(opp_shp)[names(opp_shp)=="CLUSTER"]<-"CLUSTER_ID"
head(opp_shp)

# - start iteration loop
cutoff=10
iterations=1000

store<-list()
full<-list()

for (x in 1:iterations){
  print(paste0("....starting iteration ",x, " of ", iterations))
  cwcs<-cwcs1  
  cwcs$cwcs<-sample(cwcs$cwcs) # mix up coral catch among fishing events
  
  # summarize all opportunities
  clus_byYear1<-cwcs %>% select(cwcs,CLUSTER_ID, year, timePeriod, FISHING_EV) %>%
    gather(group, catch_group, c(cwcs), factor_key = T) %>%
    group_by(CLUSTER_ID, year, timePeriod)%>%
    summarize(total_catch=sum(catch_group),
              mean_nonzero_catch=mean(catch_group[catch_group>0], na.rm=T),
              nTow=n_distinct(FISHING_EV), 
              nEnc=n_distinct(FISHING_EV[catch_group>0]),
              enc_rate=nEnc/nTow) %>%    ungroup() 
  
  n_distinct(clus_byYear1$CLUSTER_ID) #642
  
  # for the ratio calc, fill in years that have no catch/tows
  clus_byYear_fill<-clus_byYear1 
  
    for(i in 1:length(all_opps)){
      subset<-clus_byYear_fill %>% filter( CLUSTER_ID==all_opps[i]) 
      if(nrow(subset)<length(years)){ 
        clus_byYear_fill<-rbind(clus_byYear_fill, data.frame(CLUSTER_ID=all_opps[i], 
                                                             year = years[!years %in% subset$year],timePeriod=NA,
                                                             total_catch = NA, mean_nonzero_catch = NA, nTow=0, nEnc=0, enc_rate=NA))} else {} }
  clus_byYear_fill$timePeriod<-ifelse(clus_byYear_fill$year>2012, "late", "early")
  
  ## summarize  by time period
  clus_byTP1<-cwcs %>% select(cwcs,CLUSTER_ID,  timePeriod, FISHING_EV) %>%
    gather(group, catch_group, c(cwcs), factor_key = T) %>%
    group_by(CLUSTER_ID, timePeriod, group)%>%
    summarize(total_catch=sum(catch_group),
              mean_nonzero_catch=mean(catch_group[catch_group>0], na.rm=T),
              enc_mean=mean(catch_group[catch_group>0],na.rm=T),
              nTow=n_distinct(FISHING_EV), 
              nEnc=n_distinct(FISHING_EV[catch_group>0]),
              enc_rate=nEnc/nTow) %>%
    ungroup()
  
  clus_notbyTP1<-cwcs %>% select(cwcs,CLUSTER_ID,   FISHING_EV) %>%
    gather(group, catch_group, c(cwcs), factor_key = T) %>%
    group_by(CLUSTER_ID,  group)%>%
    summarize(total_catch=sum(catch_group),
              mean_nonzero_catch=mean(catch_group[catch_group>0], na.rm=T),
              enc_mean=mean(catch_group[catch_group>0],na.rm=T),
              nTow=n_distinct(FISHING_EV), 
              nEnc=n_distinct(FISHING_EV[catch_group>0]),
              enc_rate=nEnc/nTow) %>%
    ungroup()
  
  

  # -- calculate ratios
  # utilization
  Ru_allLong<-reru_fn(clus_byYear_fill, "util", "CLUSTER_ID", cutoff,opp_shp, groups="no")
  Ru_all <- Ru_allLong %>% select(CLUSTER_ID, Ru, RuBin,cutoff) 
  
  # ---- 
  
  fopps1<- Ru_all

  fopps1$iteration<-x # save iteration number
  full[[x]]<-fopps1 # save full outputs 
  
  # summarize - get average Ru within each Ru bin - ugly

  meanRu_inBin<-fopps1 %>%select( "RuBin", "Ru") %>%  group_by(RuBin) %>%
    summarize(across(everything(), mean, na.rm=T)) %>% 
    select_all(~gsub("_1", "", .))%>%
    select_all(~gsub("Ru$", "Ru_Ru", .))%>%
    pivot_longer(!RuBin,names_to=c("metric"),values_to=c("Ru_mean")) %>%
    select(!c("metric"))
  
  medianRu_inBin<-fopps1 %>%select( "RuBin", "Ru") %>%  group_by(RuBin) %>%
    summarize(across(everything(), median, na.rm=T)) %>% 
    select_all(~gsub("_1", "", .))%>%
    select_all(~gsub("Ru$", "Ru_Ru", .))%>%
    pivot_longer(!RuBin,names_to=c("metric"),values_to=c("Ru_median"))%>%
    select(!c("metric"))
  
  RuBin_summary<-merge(medianRu_inBin,meanRu_inBin,by=c("RuBin") )

  # get average encounter rate across clusters by time period
  clus_byTP<-merge(clus_byTP1, fopps1, by="CLUSTER_ID")
  clus_notbyTP<-merge(clus_notbyTP1, fopps1, by="CLUSTER_ID")

  enc_mean_by_cluster<-ddply(clus_byTP, c("RuBin","timePeriod"), summarize, mean_enc_rate=mean(enc_rate, na.rm=T), mean_total_catch=mean(total_catch, na.rm=T), mean_mean_nonzero_catch=mean(mean_nonzero_catch, na.rm=T)) %>%
    pivot_wider(id_cols = c(RuBin), names_from=timePeriod, values_from=c(mean_enc_rate, mean_total_catch, mean_mean_nonzero_catch))
  # names(enc_mean_by_cluster)<-c("RuBin","mean_enc_rate_early", "mean_enc_rate_late")
  enc_mean_by_cluster$RuBin<-factor(enc_mean_by_cluster$RuBin, levels=reLev)
  head(enc_mean_by_cluster)
  
  enc_mean_by_cluster_noTP<-ddply(clus_notbyTP, c("RuBin"), summarize, mean_enc_rate=mean(enc_rate, na.rm=T), mean_total_catch=mean(total_catch, na.rm=T), mean_mean_nonzero_catch=mean(mean_nonzero_catch, na.rm=T)) %>%
    pivot_wider(id_cols = c(RuBin), values_from=c(mean_enc_rate, mean_total_catch, mean_mean_nonzero_catch))
  
  # get global nOpp, nEnc, & nTow  
  enc_global<-ddply(clus_byTP,c('RuBin'), summarize, 
                    nOpp=n_distinct(CLUSTER_ID),
                    nEnc_early=sum(nEnc[timePeriod=="early"], na.rm=T), nEnc_late=sum(nEnc[timePeriod=="late"], na.rm=T),
                    nTow_early=sum(nTow[timePeriod=="early"], na.rm=T), nTow_late=sum(nTow[timePeriod=="late"], na.rm=T))
  
  #merge this extra info and attach to summary
  meta<-merge(enc_global, enc_mean_by_cluster,by=c("RuBin"), all.x=T, all.y=T)
  meta<-merge(meta, enc_mean_by_cluster_noTP,by=c("RuBin"), all.x=T, all.y=T)
  meta<-merge(meta,RuBin_summary, by=c("RuBin"), all.x=T, all.y=T)
  meta$iteration<-x
  
  store[[x]]<-meta # save summary for each iteration
}
# end loop


# -- combine outputs across all iterations
# summary
store_bind<-do.call("rbind",store)
# store_bind$iteration<-rep(1:iterations, each=6)

# results for each cluster for each iteration
full_bind<-do.call("rbind",full)

# fix factor levels
store_bind$RuBin<-factor(store_bind$RuBin, levels=reLev)
full_bind$RuBin<-factor(full_bind$RuBin, levels=reLev)

#write
saveRDS(store_bind, paste0(txtout, "Ru_Randomization_meanAcrossIterations_fn_100_encRate_wt.rdata"))
saveRDS(full_bind, paste0(txtout, "Ru_Randomization_detailOfIterations_fn_100_encRate_wt.rdata"))


