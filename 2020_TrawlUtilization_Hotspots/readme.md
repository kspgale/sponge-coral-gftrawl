# Changes fishing effort and catch of cold-water coral and sponge in the Pacific groundfish trawl fishery
Katie Gale, Robyn Forrest, Emily Rubidge, Chris Rooper, Jessica Nephin

## Scripts
1_gfdata-query-gftrawl.R: Uses package "gfdata" to query FOS and PacHarvTrawl. Saves output as rdata file.

2_process-gftrawl-cwcs.R: 
- filter output from 1 (remove Opt B license, NA coordinates, trips with no fishing events), rename some fields
- make line file of just fishing events (no catch), and cut according to threshold (currently 18.5 km)
- attach the cwcs name file, and convert to a site x taxon matrix with values at the species/class/order/phylum level
- ttach the matrix to the line files (with and without threshold) and write as rdata fies, and shapefile for under threshold. 

3_process-fishing-opp.R: 
- take the outputs from robyn's fishing opportunity analysis and reformat them to get shapefile with attributes
- rename some fields
- compare to my data (the pull from 1): 
		# robyn is missing 155 records that I hve, and I'm missing 19 she has. 
                # the 19 I'm missing are because  of slight measurement differences.
                # not sure why she's missing mine, but there's only 2 of them that have catch and it's small. 
- import the untrimmed line file from 2 and attach the cluster info (rdata, and shp) 
- subset to get just the lines that occur in the clusters

4_encounter-ratio-fishing-opp.R
- import the catch & lines occuring in the clusters (from 3)
- summarize catch, encounter weight, number of tows and encounters, encounter rate by cluster, by year, and by time period
- calculate the encounter ratio - for each fishing opportunity, a measure of the proportion of encounters that occurred before vs after 2012
- save rdata of spatial cluster file (642 polygons) with Re values for all 4 cwcs groups
- attach the Re values to the catch summaries 
- write plots for encounter weight (average of clusters within bin), total catch weight (total of clusters within a bin), and encounter rate (calcauted for each cluster), 
 for encounter bins and utilizaiton bins
- summarize catch, encounter weight, number of tows and encounters, encounter rate within Re/Ru bins (summary statistics on clusters, n = 642)
- write nicely-formatted table for pdf

5_encounter-ratio-hex.R
- import the lines, under threshold length, from 3
- overlay lines on a hex grid
-  summarize catch, encounter weight, number of tows and encounters, encounter rate by grid, by year, and by time period
- calculate the encounter ratio - for each hex grid, a measure of the proportion of encounters that occurred before vs after 2012
-- note this takes a lot longer than the fishing opp Re calculation because there are a lot more hexes. 
- save the encounter ratio output (mostly as shortcut for how long re takes)
- attach re bins and values to hex file and write
- summarize catch, encounter weight, number of tows and encounters, encounter rate within Re/Ru bins (summary statistics on clusters, n = nGrid)
- calculate and save area summary for pdf
- write plots of area encountered over time

6_summarize-cwcs.R
- load line file from 3, of all lines (no threshold). These are to summarize the non-spatial catch.
- summarize catch, encounter weight, number of tows and encounters, encounter rate by year and by time period (no sub-unit, just global within year/tp)
- calculate summary statistics (average, sd, median) for years within time period (`annAvg`)
- save the byYear, byTP, and annAvg outputs for the pdf
- output summary plots on encounter weight, rate, etc.

Plot sources:
- Fig 1- ArcMap
- Fig 2- `6_summarize-cwcs.R`, combine with `plotsToPlate_addText.R`
- Fig 3- ArcMap
- Fig 4- `6_summarize-cwcs.R`, combine with `plotsToPlate_addText.R`
- Fig 5- `reformat_robyns_figs.R`
- Fig 6- `reformat_robyns_figs.R`
- Fig S1- From Robyn
- Fig S2- From Robyn
- Fig S3- `reformat_robyns_figs.R`
- Fig S4- `4_util-ratio-threshold-sensitivity.R`
- Fig S5- `7_cwcs-distribution-rescale.R`
- Fig S6- `7_cwcs-distribution-rescale.R`
